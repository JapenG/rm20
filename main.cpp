/*----------------------------------------------
\@file   main.cpp
\@brief
\@author Liaojiapeng
*-----------------------------------------------*/
#include <iostream>
#include<opencv2/opencv.hpp>
#include"ArmorDetector.h"
using namespace cv;
using namespace std;
#define ARMOR_DEVICE 0
bool CamerRead(ArmorDetector& armor_param);
int main()
{
    ArmorDetector armor;
    armor.CamaraInit(ARMOR_DEVICE);
    //VideoCapture capture("D:\\vscpp\\imcaproject2\\第二题.avi");//加载本地视频
    //armor.armorCapture = capture;
    while (1)
    {
        if (!CamerRead(armor))
            continue;
        armor.AutoShoot();
    }
    return 0;
}
bool CamerRead(ArmorDetector& armor_param)
{
    armor_param.armorCapture.read(armor_param.armorImg);
    if (!armor_param.armorImg.data)
    {
        cout << "No date" << endl;
        armor_param.CamaraInit(ARMOR_DEVICE);//视觉辅助摄像头初始化
        return false;
    }
    else
        return true;
}