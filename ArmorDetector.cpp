//
// Created by dl1ja12 on 20-1-13.
//

#include "ArmorDetector.h"
#define T_ANGLE_THRE 5
#define T_ANGLE_THRE180 3
#define T_ANGLE_THREMIN 3
#define T_ANGLE_THRE180MIN 2
#define T_HIGH_RAT 0.2
#define T_HIGH_RAT_ANGLE 0.34
#define T_WHIDTH_RAT 0.4
#define T_WHIDTH_RAT_ANGLE 0.55
#define L_WH_RAT 0.8
#define IMG_CENTER_X 320//与摄像头有关 图像中心点？（60.0*18.0
#define IMG_CENTER_Y 240

using namespace cv;
using namespace std;



ArmorDetector::ArmorDetector()
{
    ourTeamColor = TEAMRED;
}



bool ArmorDetector::CamaraInit(int device)
{
    armorCapture.open(device);
    if (!armorCapture.isOpened())
    {
        cout << "Cannot get the camera" << endl;
        return false;
    }
    else
        return true;
}


void ArmorDetector::ImgPreprocess(const Mat& src, Mat& dst)
{
    vector<Mat>img_channels;//通道
    split(src, img_channels);//通道分离
    if (ourTeamColor)//我红
    {
        Mat blueChannelsImg;
        blueChannelsImg = img_channels.at(0);//蓝色通道
        imshow("bluechannels", blueChannelsImg);
        blueChannelsImg = blueChannelsImg - img_channels.at(1)*0.4 - img_channels.at(2)*0.4;//去除白色噪点
        //imshow("blue", blueChannelsImg);
        blur(blueChannelsImg, blueChannelsImg, Size(3, 3));
        blueChannelsImg = blueChannelsImg * 3;
        //imshow("blue",blueChannelsImg);
        double maxgrayImg;
        minMaxLoc(blueChannelsImg, 0, &maxgrayImg, 0, 0);
        Mat binImg;
        threshold(blueChannelsImg, binImg, maxgrayImg*0.7, 255, THRESH_BINARY);
        //imshow("erzhihua", binImg);
        Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));
        //消除杂点 具现化目标
        dilate(binImg, dst, element, Point(-1, -1), 3);//膨胀3次
        erode(dst, dst, element, Point(-1, -1), 4);//腐蚀4次
        dst = binImg;
        imshow("rgbyuchuli", dst);
        //Mat gray_src;
        //cvtColor(srcImg, gray_src, CV_RGB2GRAY);
        ////imshow("gray",gray_src);
        //medianBlur(gray_src, gray_src, 3);
        //Mat t_src;
        //threshold(gray_src, t_src, 130, 255, CV_THRESH_BINARY);
        ////膨胀后寻找轮廓,及绘制
        //Mat element = getStructuringElement(MORPH_RECT, Size(13, 13));
        //dilate(t_src, dstImg, element);

    }
    else//我蓝
    {

        Mat redChannelsImg;
        redChannelsImg = img_channels.at(2);//红色通道
        imshow("redchannels", redChannelsImg);
        redChannelsImg = redChannelsImg - img_channels.at(0)*0.4 - img_channels.at(1)*0.4;//去除白色噪点（0.4可改）
        //redChannelsImg = redChannelsImg - img_channels.at(0);//官方思路
        blur(redChannelsImg, redChannelsImg,Size(3,3));
        redChannelsImg = redChannelsImg * 3;//增强对比度
        imshow("red", redChannelsImg);
        double maxgrayImg;
        minMaxLoc(redChannelsImg, 0, &maxgrayImg, 0, 0);//获取最大灰度值
        Mat binImg;
        threshold(redChannelsImg, binImg, maxgrayImg*0.7, 255,THRESH_BINARY);//第三个参数0.7可改
        imshow("erzhihua", binImg);
        Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));
        //消除杂点 具现化目标
        dilate(binImg, dst, element, Point(-1, -1), 3);//膨胀3次
        erode(dst, dst, element, Point(-1, -1), 4);//腐蚀4次
        dst = binImg;
        imshow("rgbyuchuli", dst);

    }

}






//Input:srcImg dstImg
//Output : 视野目标 击打目标
void ArmorDetector::FindArmor(Mat& srcImg, Mat& dstImg, vector<RotatedRect>& all, RotatedRect& target)
{
    all.clear();
    target.center.x = 0;
    target.center.y = 0;
    target.size.width = 0;
    target.size.height = 0;
    target.angle = 0;

    RotatedRect s, s_fitEllipse, s_minAreaRect;//用于筛选轮廓
    vector<RotatedRect> ss;//筛选完存放
    ss.clear();

    vector<vector<Point>> contours;
    vector<Vec4i>hierarchy;

    findContours(dstImg, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);//寻找轮廓
    Mat drawing = Mat::zeros(dstImg.size(), CV_8UC3);
    RNG g_rng(12345);
    for (int i = 0; i < contours.size(); i++)
    {
        Scalar color = Scalar(g_rng.uniform(0, 255), g_rng.uniform(0, 255), g_rng.uniform(0, 255));
        drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());//画出所有轮廓
        imshow("contours", drawing);

        if (contours[i].size() >= 10)
        {
            s_fitEllipse = fitEllipse(contours[i]);//椭圆拟合
            s_minAreaRect = minAreaRect(contours[i]);

            s.angle = s_fitEllipse.angle;
            s.center = s_fitEllipse.center;
            if (s_minAreaRect.size.width > s_minAreaRect.size.height)
            {
                s.size.height = s_minAreaRect.size.width;
                s.size.width = s_minAreaRect.size.height;
            }
            else
            {
                s.size.width = s_minAreaRect.size.width;
                s.size.height = s_minAreaRect.size.height;
            }

            if ((s.size.width / s.size.height) > L_WH_RAT)//报错可转化宏定义方式
                continue;
            int x = s.center.x - s.size.width;
            if (x < 0)
                continue;
            int y = s.center.y - s.size.height;
            if (y < 0)
                continue;
            int w = s.size.width + s.size.width;
            if (w > dstImg.cols - x)
                continue;
            int h = s.size.height + s.size.height;
            if (h > dstImg.rows - y)
                continue;

            if ((s.angle < 45 || s.angle>135) && (s.size.height > 10) && (s.size.height < 150))
                ss.push_back(s);
        }
    }
    //判别装甲
    vector<RotatedRect>armors;
    vector<ArmorBuilded>armor_SECOND;
    ArmorBuilded armor_FIRST;
    static float armor_center_x;
    static float armor_center_y;

    armors.clear();
    armor_SECOND.clear();
    int nL, nW;

    if (ss.size() < 2)//旋转矩形小于两个，直接返回
    {
        target.center.x = 0;
        target.center.y = 0;
        target.size.width = 0;
        target.size.height = 0;
        target.angle = 0;
        all.push_back(target);
        armor_center_x = 0;
        armor_center_y = 0;
    }
    else
    {   //两两匹配
        for (int i = 0; i < ss.size() - 1; i++)
        {
            for (int j = i + 1; j < ss.size(); j++)
            {
                double height_diff = abs(ss[i].size.height - ss[j].size.height);
                double height_sum = ss[i].size.height + ss[j].size.height;
                double width_diff = abs(ss[i].size.width - ss[j].size.width);
                double width_sum = ss[i].size.width + ss[j].size.width;
                double angle_diff = fabs(ss[i].angle - ss[j].angle);
                double Y_diff = abs(ss[i].center.y - ss[j].center.y);
                double X_diff = abs(ss[i].center.x - ss[j].center.x);
                double MH_diff = (min(ss[i].size.height, ss[j].size.height)) * 2 / 3;
                double height_max = max(ss[i].size.height, ss[j].size.height);

                if (Y_diff < MH_diff&&X_diff < height_max * 4 &&
                    (angle_diff < T_ANGLE_THRE || 180 - angle_diff < T_ANGLE_THRE180) &&
                    height_diff / height_sum < T_HIGH_RAT&&
                    width_diff / width_sum < T_WHIDTH_RAT)
                {
                    armor_FIRST.armorS.center.x = ((ss[i].center.x + ss[j].center.x) / 2);
                    armor_FIRST.armorS.center.y = ((ss[i].center.y + ss[j].center.y) / 2);
                    armor_FIRST.armorS.angle = ((ss[i].angle + ss[j].angle) / 2);
                    if (180 - angle_diff < T_ANGLE_THRE180)
                        armor_FIRST.armorS.angle += 90;
                    nL = (ss[i].size.height + ss[j].size.height) / 2;
                    nW = sqrt((ss[i].center.x - ss[j].center.x)*(ss[i].center.x - ss[j].center.x)
                              +(ss[i].center.y - ss[j].center.y)*(ss[i].center.y - ss[j].center.y));
                    if (nL < nW)
                    {
                        armor_FIRST.armorS.size.height = nL;
                        armor_FIRST.armorS.size.width = nW;
                    }
                    else
                    {
                        armor_FIRST.armorS.size.height = nW;
                        armor_FIRST.armorS.size.width = nL;
                    }
                    if (Y_diff < nW / 3)
                    {
                        armor_FIRST.buils1_No = i;
                        armor_FIRST.buils2_No = j;
                        armor_FIRST.build_features[0] = angle_diff;
                        armor_FIRST.build_features[1] = Y_diff;
                        armor_FIRST.build_features[2] = height_diff;
                        armor_FIRST.build_features[3] = width_diff;
                        armor_SECOND.push_back(armor_FIRST);
                    }
                }
                else if ((angle_diff < T_ANGLE_THREMIN || 180 - angle_diff < T_ANGLE_THREMIN) &&
                         Y_diff < MH_diff * 3 / 2 && X_diff < height_max * 4 &&
                         width_diff / width_sum < T_WHIDTH_RAT_ANGLE)
                {
                    armor_FIRST.armorS.center.x = ((ss[i].center.x + ss[j].center.x) / 2);
                    armor_FIRST.armorS.center.y = ((ss[i].center.y + ss[j].center.y) / 2);
                    armor_FIRST.armorS.angle = ((ss[i].angle + ss[j].angle) / 2);
                    if (180 - angle_diff < T_ANGLE_THRE180)
                        armor_FIRST.armorS.angle += 90;
                    nL = (ss[i].size.height + ss[j].size.height) / 2;
                    nW = sqrt((ss[i].center.x - ss[j].center.x)*(ss[i].center.x - ss[j].center.x)
                              + (ss[i].center.y - ss[j].center.y)*(ss[i].center.y - ss[j].center.y));
                    if (nL < nW)
                    {
                        armor_FIRST.armorS.size.height = nL;
                        armor_FIRST.armorS.size.width = nW;
                    }
                    else
                    {
                        armor_FIRST.armorS.size.height = nW;
                        armor_FIRST.armorS.size.width = nL;
                    }
                    if (Y_diff < nW / 2)
                    {
                        armor_FIRST.buils1_No = i;
                        armor_FIRST.buils2_No = j;
                        armor_FIRST.build_features[0] = angle_diff;
                        armor_FIRST.build_features[1] = Y_diff;
                        armor_FIRST.build_features[2] = height_diff;
                        armor_FIRST.build_features[3] = width_diff;
                        armor_SECOND.push_back(armor_FIRST);
                    }
                }
                else if ((angle_diff < 3 || 180 - angle_diff < 2) &&
                         Y_diff < MH_diff * 2 && X_diff < height_max * 4)
                {
                    armor_FIRST.armorS.center.x = ((ss[i].center.x + ss[j].center.x) / 2);
                    armor_FIRST.armorS.center.y = ((ss[i].center.y + ss[j].center.y) / 2);
                    armor_FIRST.armorS.angle = ((ss[i].angle + ss[j].angle) / 2);
                    if(180-angle_diff<T_ANGLE_THRE180)
                        armor_FIRST.armorS.angle += 90;
                    nL = (ss[i].size.height + ss[j].size.height) / 2;
                    nW = sqrt((ss[i].center.x - ss[j].center.x)*(ss[i].center.x - ss[j].center.x)
                              + (ss[i].center.y - ss[j].center.y)*(ss[i].center.y - ss[j].center.y));
                    if (nL < nW)
                    {
                        armor_FIRST.armorS.size.height = nL;
                        armor_FIRST.armorS.size.width = nW;
                    }
                    else
                    {
                        armor_FIRST.armorS.size.height = nW;
                        armor_FIRST.armorS.size.width = nL;
                    }
                    if ((abs(ss[i].center.y-ss[j].center.y)<nW/2))
                    {
                        armor_FIRST.buils1_No = i;
                        armor_FIRST.buils2_No = j;
                        armor_FIRST.build_features[0] = angle_diff;
                        armor_FIRST.build_features[1] = Y_diff;
                        armor_FIRST.build_features[2] = height_diff;
                        armor_FIRST.build_features[3] = width_diff;
                        armor_SECOND.push_back(armor_FIRST);
                    }
                }
                else if ((angle_diff < 3 || 180 - angle_diff < 2) &&
                         Y_diff < MH_diff * 3 && X_diff < height_max * 5)
                {
                    armor_FIRST.armorS.center.x = ((ss[i].center.x + ss[j].center.x) / 2);
                    armor_FIRST.armorS.center.y = ((ss[i].center.y + ss[j].center.y) / 2);
                    armor_FIRST.armorS.angle = ((ss[i].angle + ss[j].angle) / 2);
                    if (180 - angle_diff < T_ANGLE_THRE180)
                        armor_FIRST.armorS.angle += 90;
                    nL = (ss[i].size.height + ss[j].size.height) / 2;
                    nW = sqrt((ss[i].center.x - ss[j].center.x)*(ss[i].center.x - ss[j].center.x)
                              + (ss[i].center.y - ss[j].center.y)*(ss[i].center.y - ss[j].center.y));
                    if (nL < nW)
                    {
                        armor_FIRST.armorS.size.height = nL;
                        armor_FIRST.armorS.size.width = nW;
                    }
                    else
                    {
                        armor_FIRST.armorS.size.height = nW;
                        armor_FIRST.armorS.size.width = nL;
                    }
                    if (Y_diff<nW / 2)
                    {
                        armor_FIRST.buils1_No = i;
                        armor_FIRST.buils2_No = j;
                        armor_FIRST.build_features[0] = angle_diff;
                        armor_FIRST.build_features[1] = Y_diff;
                        armor_FIRST.build_features[2] = height_diff;
                        armor_FIRST.build_features[3] = width_diff;
                        armor_SECOND.push_back(armor_FIRST);
                    }
                }


            }
        }
        if (armor_SECOND.size() < 1)
        {
            cout << "No armor" << endl;

        }
        else if (armor_SECOND.size() == 1)
        {
            target = armor_SECOND[0].armorS;
            all.push_back(armor_SECOND[0].armorS);
            armor_center_x = target.center.x;
            armor_center_y = target.center.y;
        }
        else//不止一个装甲
        {
            double min_feature = 9999999;
            for (int armor_i = 0; armor_i < armor_SECOND.size(); armor_i++)
            {
                armors.push_back(armor_SECOND[armor_i].armorS);
                double feature = armor_SECOND[armor_i].build_features[0] * 100 +
                                 armor_SECOND[armor_i].build_features[1] * 10 +
                                 armor_SECOND[armor_i].build_features[2] * 100 +
                                 abs(armor_SECOND[armor_i].armorS.center.x - armor_center_x) * 50 +
                                 abs(armor_SECOND[armor_i].armorS.center.y - armor_center_y) * 50 -
                                 armor_SECOND[armor_i].armorS.size.height * 100 -
                                 armor_SECOND[armor_i].armorS.size.width * 100;
                if (feature < min_feature)
                {
                    min_feature = feature;
                    target = armor_SECOND[armor_i].armorS;
                }


            }
            armor_center_x = target.center.x;
            armor_center_y = target.center.y;
            all = armors;

        }
    }
}





void DrawAll(vector<RotatedRect>rect, Mat img)
{
    for (int i = 0; i < rect.size(); i++)
    {
        Point2f pp[4];
        rect[i].points(pp);//计算二维盒子顶点
        line(img, pp[0], pp[1], CV_RGB(255, 0, 0), 2, 8, 0);
        line(img, pp[1], pp[2], CV_RGB(255, 0, 0), 2, 8, 0);
        line(img, pp[2], pp[3], CV_RGB(255, 0, 0), 2, 8, 0);
        line(img, pp[3], pp[0], CV_RGB(255, 0, 0), 2, 8, 0);
    }
}

/*void DrawTarget(RotatedRect box, Mat img)
{
	Point2f pts[8];
	pts[0].x = box.center.x;
	pts[0].y = box.center.y - 10;
	pts[1].x = box.center.x;
	pts[1].y = box.center.y + 10;
	pts[2].x = box.center.x - 10;
	pts[2].y = box.center.y;
	pts[3].x = box.center.x + 10;
	pts[3].y = box.center.y;
	pts[4].x = IMG_CENTER_X;
	pts[4].y = IMG_CENTER_Y - 10;
	pts[5].x = IMG_CENTER_X;
	pts[5].y = IMG_CENTER_Y + 10;
	pts[6].x = IMG_CENTER_X - 10;
	pts[6].y = IMG_CENTER_Y;
	pts[7].x = IMG_CENTER_X + 10;
	pts[7].y = IMG_CENTER_Y;
	line(img, pts[0], pts[1], CV_RGB(0, 255, 0), 2, 8, 0);
	line(img, pts[2], pts[3], CV_RGB(0, 255, 0), 2, 8, 0);
	line(img, pts[4], pts[5], CV_RGB(255, 255, 255), 2, 8, 0);
	line(img, pts[6], pts[7], CV_RGB(255, 255, 255), 2, 8, 0);

}*/



void ArmorDetector::AutoShoot()
{
    //armorImg=imread("");//加载本地图片
    ImgPreprocess(armorImg, preImg);
    FindArmor(armorImg, preImg, allTarget, hitTarget);
    DrawAll(allTarget, armorImg);
    //DrawTarget(hitTarget, armorImg);
    imshow("原图", armorImg);
    //imshow("预处理图", preImg);
    waitKey(1);//tupian0 shiping1
}