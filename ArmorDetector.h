//
// Created by dl1ja12 on 20-1-13.
//

#ifndef RM20_ARMORDETECTOR_H
#define RM20_ARMORDETECTOR_H
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <vector>
#include <stdlib.h>

using namespace cv;
using namespace std;
#define TEAMBLUE 0;
constexpr auto TEAMRED = 1;



class ArmorDetector
{
public:
    Mat armorImg;//视觉辅助原图
    Mat preImg;//预处理图像
    VideoCapture armorCapture;//视觉辅助摄像头
    bool CamaraInit(int device);//初始化摄像头
    int ourTeamColor;//红蓝
    ArmorDetector();
    void AutoShoot();
private:
    vector<RotatedRect> allTarget;
    RotatedRect hitTarget;
    void FindArmor(Mat& src, Mat& dst, vector<RotatedRect>& all, RotatedRect& target);//定位装甲板
    void ImgPreprocess(const Mat& src, Mat& dst);//图像预处理
};



class ArmorBuilded//装甲构成
{
public:
    RotatedRect armorS;
    int buils1_No = 0;
    int buils2_No = 0;
    int build_features[4];//角度差 高度坐标差 高度差 宽度差

};


#endif //RM20_ARMORDETECTOR_H
